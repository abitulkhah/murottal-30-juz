import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AdMobFree } from '@ionic-native/admob-free';
import { MusicControls } from '@ionic-native/music-controls';
import { Media } from '@ionic-native/media';
import { DetailSuratPage } from '../pages/detail-surat/detail-surat';
import { JuzAmmaPage } from '../pages/juz-amma/juz-amma';
import { AllSuratPage } from '../pages/all-surat/all-surat';
import { AllJuzPage } from '../pages/all-juz/all-juz';
import { AllSuratServiceProvider } from '../providers/all-surat-service/all-surat-service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CategoryPage } from './../pages/category/category';
import { IonicAudioModule, WebAudioProvider, CordovaMediaProvider } from 'ionic-audio';
import { NativeAudio } from '@ionic-native/native-audio';
import { DialogserviceProvider } from '../providers/dialogservice/dialogservice';
import { AppVersion } from '@ionic-native/app-version';
import { Keyboard } from '@ionic-native/keyboard';
import { AppMinimize } from '@ionic-native/app-minimize';
import { AppRate } from '@ionic-native/app-rate';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Network } from '@ionic-native/network';
import { Toast } from '@ionic-native/toast';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ShareAppProvider } from '../providers/share-app/share-app';
import { BackgroundMode } from '@ionic-native/background-mode';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    DetailSuratPage,
    JuzAmmaPage,
    AllSuratPage,
    AllJuzPage,
    CategoryPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicAudioModule.forRoot(myCustomAudioProviderFactory),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    DetailSuratPage,
    JuzAmmaPage,
    AllSuratPage,
    AllJuzPage,
    CategoryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AdMobFree,
    Media,
    NativeAudio,
    HttpClientModule,
    HttpClient,
    AllSuratServiceProvider,
    DialogserviceProvider,
    MusicControls,
    AppVersion,
    Keyboard,
    AppMinimize,
    AppRate,
    LocalNotifications,
    Network,
    Toast,
    SocialSharing,
    ShareAppProvider,
    BackgroundMode
  ]
})
export class AppModule {}
export function myCustomAudioProviderFactory() {
  return (window.hasOwnProperty('cordova')) ? new CordovaMediaProvider() : new WebAudioProvider();
}
