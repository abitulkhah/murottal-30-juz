import { Toast } from '@ionic-native/toast';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/*
  Generated class for the DialogserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DialogserviceProvider {

  constructor(public http: HttpClient,private alertCtrl: AlertController,private toast:Toast) {
  }

  confirmAlert(title: string, message: string, handler: any) {
    let confirm = this.alertCtrl.create({
      title: title,
      message: message,
      cssClass:'alertCustomCss',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "OK",
          handler: handler
        }
      ]
    });
    confirm.present();
  }

  dialogCancel(title: string, message: string, okHandler: any,cancelHandler:any) {
    let confirmDialogCancel = this.alertCtrl.create({
      title: title,
      message: message,
      cssClass:'alertCustomCss',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "Cancel",
          handler: cancelHandler
        },
        {
          text: "OK",
          handler: okHandler
        }
      ]
    });
    confirmDialogCancel.present();
  }

  toastShow(message,duration,position){
    this.toast.show(message, duration,position).subscribe(toast => {
      console.log(toast);
    });
  }


}
