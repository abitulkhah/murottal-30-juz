import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AllSuratPage } from '../all-surat/all-surat';
import { AllJuzPage } from '../all-juz/all-juz';
import { ShareAppProvider } from '../../providers/share-app/share-app';

/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {

  allCategory = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,private shareApp: ShareAppProvider) {

    this.allCategory = [
      {
        id: 1,
        name: "Juz",
        count: "30 Juz",
        icon: "assets/imgs/icon-alquran.png",
      },
      {
        id: 2,
        name: "Semua Surat",
        count: "114 Surat",
        icon: "assets/imgs/icon-alquran.png",
      },
      {
        id: 3,
        name: "Surat Makiyah",
        count: "88 Surat",
        icon: "assets/imgs/icon-alquran.png",
      },
      {
        id: 4,
        name: "Surat Madaniyah",
        count: "26 Surat",
        icon: "assets/imgs/icon-alquran.png",
      }];
  }

  shareAppOption(){
    this.shareApp.presentActionSheet();
  }

  DetailCategory(params){
    if(params.id ==1){
      this.navCtrl.push(AllJuzPage,{title:params.name});
    }else{
      this.navCtrl.push(AllSuratPage,{title:params.name, category:params.id , dataParams : ""});
    }
  }
}
