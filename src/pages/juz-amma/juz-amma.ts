import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AllSuratServiceProvider } from '../../providers/all-surat-service/all-surat-service';
import { DetailSuratPage } from '../detail-surat/detail-surat';
import { ShareAppProvider } from '../../providers/share-app/share-app';

/**
 * Generated class for the JuzAmmaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-juz-amma',
  templateUrl: 'juz-amma.html',
})
export class JuzAmmaPage {
  allListSurat;
  allData;
  showTitle : boolean = true;
  searchInput;

  constructor(public navCtrl: NavController, public navParams: NavParams,private allSuratService: AllSuratServiceProvider,private shareApp: ShareAppProvider) {
      this.allSuratService.getData(5).then((res)=>{
      this.allListSurat = this.responseData(res);
      this.allData = this.responseData(res);
    });
  }


  responseData(res){
    let response = [];
    for (const i in res) {
      response.push({
        id : res[i].id,
        indexToPlay : parseInt(i),
        suratName :res[i].suratName,
        fileName: res[i].fileName,
        juz: res[i].juz,
        isMakiyah: res[i].isMakiyah,
        countAyat: res[i].countAyat,
        suratMeaning: res[i].suratMeaning,
        isChoosen : res[i].isChoosen,
        arabSuratName: res[i].arabSuratName
      });
    }
    return response;
  }

  ionViewWillLeave(){
    this.showTitle = true;
    this.searchInput ="";
  }

  DetailSurat(params){
    this.navCtrl.push(DetailSuratPage,{title:params.title,paramsData: this.allData,indexToPlay:params.indexToPlay,requestToPlay:true});
  }

  Searching(){
    this.showTitle = false;
  }

  onInput(event){
    this.allListSurat = this.allData.filter((data)=>{
      return data.suratName.toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1;
    });
  }

  onCancel(event){
    this.showTitle = true;
  }

  shareAppOption(){
    this.shareApp.presentActionSheet();
  }
}
