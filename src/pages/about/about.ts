import { AppVersion } from '@ionic-native/app-version';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ShareAppProvider } from '../../providers/share-app/share-app';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  contentsData;
  versionApps;
  generation;
  address;
  fullName;
  email;
  suggestion;

  constructor(public navCtrl: NavController,private appVersion: AppVersion, private shareApp: ShareAppProvider) {

    this.appVersion.getVersionNumber().then((version)=>{
      this.versionApps = version;
    });

    this.contentsData =[{
      description:"1. Menu Home : menampilkan semua surat dari Alquran pada saat melakukan pencarian"
    },{
      description:"2. Menu Kategori : menampilkan pilihan dalam Juz dan Semua Surat dalam Alquran."
    },{
      description:"3. Menu Juz Amma : menampilkan surat surat dari juz 30."
    },{
      description:"4. Menu Pencarian : menampilkan surat yang dicari."
    },{
      description:"5. Detail Surat : menampilkan informasi dari surat yang sedang di dengarkan."
    }];

    this.generation = "Lulusan : Pondok Pesantren Muhammadiyah Kudus Angkatan ke -4 (Islamic Boarding School Muhammadiyah)";
    this.address = "Alamat : Tangerang";
    this.fullName = "Abi Tulkhah";
    this.email = "abimeiiku@gmail.com";
    this.suggestion = "anda bisa memberikan review aplikasi di playstore atau bisa email ke abimeiiku@gmail.com";

  }
  shareAppOption(){
    this.shareApp.presentActionSheet();
  }

  ionViewWillLeave(){
    this.shareApp.ratingApps();
  }
}
