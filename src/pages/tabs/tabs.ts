import { CategoryPage } from './../category/category';
import { JuzAmmaPage } from './../juz-amma/juz-amma';
import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = CategoryPage;
  tab3Root = JuzAmmaPage;
  tab4Root = AboutPage;
  constructor() {

  }
}
