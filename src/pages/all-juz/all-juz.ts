import { AllSuratPage } from './../all-surat/all-surat';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AllSuratServiceProvider } from '../../providers/all-surat-service/all-surat-service';
import { ShareAppProvider } from '../../providers/share-app/share-app';

/**
 * Generated class for the AllJuzPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-all-juz',
  templateUrl: 'all-juz.html',
})
export class AllJuzPage {
  allJuzz;
  constructor(public navCtrl: NavController, public navParams: NavParams,private allSuratService: AllSuratServiceProvider,private shareApp: ShareAppProvider) {
    this.allSuratService.getData(6).then((res)=>{
      this.allJuzz = res;
    });
  }
  shareAppOption(){
    this.shareApp.presentActionSheet();
  }

  DetailJuz(params){
    this.navCtrl.push(AllSuratPage,{title:params.juzName, category: 7 , dataParams : params.filteredDataSurat});
  }

}
