import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig, AdMobFreeRewardVideoConfig } from '@ionic-native/admob-free';

/*
  Generated class for the AllSuratServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AllSuratServiceProvider {

  recordPlay = 0;
  constructor(public http: HttpClient,private adMobFree: AdMobFree) {

  }

  getData(params){
      let responseData;
      return new Promise(resolve => {
        this.http.get("assets/murottal/allsurat.json").subscribe(data => {
                responseData = data;
                switch(params){
                  /* surat makiyah*/
                case 1:
                      responseData;
                break;
                /* surat makiyah*/
                case 3:
                      responseData = responseData.filter((data)=>{
                          return data.isMakiyah;
                      });
                      for (var i in responseData) {
                          responseData.push({
                            id : responseData[i].id,
                            indexToPlay : i,
                            suratName :responseData[i].suratName,
                            fileName: responseData[i].fileName,
                            juz: responseData[i].juz,
                            isMakiyah: responseData[i].isMakiyah,
                            countAyat: responseData[i].countAyat,
                            suratMeaning: responseData[i].suratMeaning,
                            isChoosen : responseData[i].isChoosen,
                            arabSuratName: responseData[i].arabSuratName
                          });
                      }
                      break;
                /* surat madaniyah*/
                case 4:
                      responseData = responseData.filter((data)=>{
                          return !data.isMakiyah;
                      });
                      break;
                /* surat juz amma*/
                case 5:
                      responseData = responseData.filter((data)=>{
                          return data.juz[0]==30;
                      });
                      break;
               /* surat 30 juz*/
                case 6:
                      let allJuzData = [];
                      for (let i = 0; i < 30; i++) {
                        let filteredDataSurat = [];
                        responseData.forEach(data => {
                          if(data.juz[0]==(i+1)){
                            filteredDataSurat.push(data);
                          }else if(data.juz[1]==(i+1)){
                            filteredDataSurat.push(data);
                          }else if( data.juz[2]==(i+1)){
                            filteredDataSurat.push(data);
                          }
                        });

                        allJuzData.push({
                            juzName : "Juz "+(i+1),
                            filteredDataSurat : filteredDataSurat
                        });
                      }
                      responseData = allJuzData;
                    break;
                case 7:
                    responseData = responseData.filter((data)=>{
                        return data.isChoosen;
                });
                    break;
                default:
                   responseData;
                break;
         }

        resolve(responseData);

      }, err => {
          console.log(err);
        });
      });
  }

  setRecordPlaying(recordPlay){
      this.recordPlay = recordPlay;
  }

  getRecordPlaying(){
    return this.recordPlay;
  }

  async showBannerAd() {
    try {
      const bannerConfig: AdMobFreeBannerConfig = {
        id: "ca-app-pub-8685901438221477/6670588479",
        isTesting: true,
        autoShow: true
      }

      this.adMobFree.banner.config(bannerConfig);
      const result = await this.adMobFree.banner.prepare();
      console.log(result);
    }
    catch (e) {
      console.error(e);
    }
  }

  async showInterstitialAd() {
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: "ca-app-pub-8685901438221477/6510806370",
        isTesting: true,
        autoShow: true
      }

      this.adMobFree.interstitial.config(interstitialConfig);
      const result = await this.adMobFree.interstitial.prepare();
      console.log(result);
    }
    catch (e) {
      console.error(e)
    }
  }

  async showVideoRewardsAd() {
    try {
      const videoRewardsConfig: AdMobFreeRewardVideoConfig = {
        id: "ca-app-pub-8685901438221477/6123793563",
        isTesting: true,
        autoShow: true
      }

      this.adMobFree.rewardVideo.config(videoRewardsConfig);
      const result = await this.adMobFree.rewardVideo.prepare();
      console.log(result);
    }
    catch (e) {
      console.error(e);
    }
  }

}
