import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular/util/events';
import { ShareAppProvider } from '../../providers/share-app/share-app';
import { App } from 'ionic-angular/components/app/app';

/**
 * Generated class for the DetailSuratPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail-surat',
  templateUrl: 'detail-surat.html',
})
export class DetailSuratPage {

  indexToPlay;
  isRepeat : boolean = false;
  isRandom : boolean = false;
  iconMediaPlaying: String = "ios-play";
  allData;
  suratName;
  suratMeaning;
  countAyat;
  suratCategory;
  randomClass = "iconNewCustom";
  repeatClass = "iconNewCustom";
  isPlaying = false;
  requestToPlay;
  detailSuratContent;

  constructor(private navParams: NavParams, private events: Events,private shareApp: ShareAppProvider,private app: App) {
    this.allData = this.navParams.get("paramsData");
    this.indexToPlay = this.navParams.get("indexToPlay");
    this.requestToPlay = this.navParams.get("requestToPlay");
    this.detailSuratContent = this.allData[this.indexToPlay];
    this.detailSuratContent.suratCategory = this.allData[this.indexToPlay].isMakiyah? "Makiyah" : "Madaniyah";
    this.events.publish("murottal:isPlaying",true,true,this.allData,0,this.indexToPlay);
    if(this.requestToPlay){
        this.playorpause();
    }else{
      this.isPlaying = true;
      this.iconMediaPlaying = "ios-pause";
    }

    this.events.subscribe("murottal:detailSurat", (detailSuratContent,firstOrlatestSurat) => {
        this.detailSuratContent = {};
        this.detailSuratContent = detailSuratContent;
        if(firstOrlatestSurat){
          this.iconMediaPlaying = "ios-play";
        }
    });
  }

  ionViewWillLeave(){
    this.app.navPop();
    this.sendEventsToMyApp(0,false);
  }

  playRandom(){
    if(this.isRandom){
      this.randomClass = "iconNewCustom";
      this.isRandom = false;
    }else{
      this.randomClass = "iconNewCustom bold";
      this.repeatClass =  "iconNewCustom";
      this.isRandom = true;
      this.isRepeat = false;
    }
    this.sendEventsToMyApp(1,true);
  }

  previous(){
    this.iconMediaPlaying = "ios-pause";
    this.isPlaying = true;
    this.sendEventsToMyApp(2,true);
  }

  playorpause(){
    this.sendEventsToMyApp(3,true);
    if(this.isPlaying){
      this.isPlaying = false;
      this.iconMediaPlaying = "ios-play";
    }else{
      this.isPlaying = true;
      this.iconMediaPlaying = "ios-pause";
    }
  }

  next(){
    this.isPlaying = true;
    this.iconMediaPlaying = "ios-pause";
    this.sendEventsToMyApp(4,true);
  }

  playRepeat(){
      if(this.isRepeat){
        this.repeatClass = "iconNewCustom";
        this.isRepeat = false;
      }else{
        this.repeatClass =  "iconNewCustom bold";
        this.randomClass =  "iconNewCustom";
        this.isRepeat = true;
        this.isRandom = false;
      }
      this.sendEventsToMyApp(5,true);
  }

  sendEventsToMyApp(typeOfPlaying,isDetailPage){
    this.events.publish("murottal:isPlaying",true,isDetailPage,this.allData,typeOfPlaying,this.indexToPlay,this.requestToPlay);
    if(this.requestToPlay){
      this.requestToPlay = false;
    }
  }

  shareAppOption(){
    this.shareApp.presentActionSheet();
  }

}
