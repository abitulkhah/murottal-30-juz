import { Keyboard } from '@ionic-native/keyboard';
import { Events } from 'ionic-angular/util/events';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { AudioProvider } from 'ionic-audio';
import { MusicControls } from '@ionic-native/music-controls';
import { AllSuratServiceProvider } from '../providers/all-surat-service/all-surat-service';
import { AppMinimize } from '@ionic-native/app-minimize';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { DialogserviceProvider } from '../providers/dialogservice/dialogservice';
import { BackgroundMode } from '@ionic-native/background-mode';
import { App } from 'ionic-angular/components/app/app';
import { ViewChild } from '@angular/core/src/metadata/di';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  myTracks=[];
  showNavigation = false;
  isDetailSurat = false;
  indexToPlay=-1;
  selectedTrack;
  allData;
  progressBar;
  isRepeat = false;
  isRandom = false;
  iconMediaPlaying = "ios-play";
  tempRandomArray = [];
  navigationPlaying="floatingPlayButton";
  currentView="rootPage";
  @ViewChild("audio") audio;
  banner;
  interstitial;
  reward;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private events: Events, private audioProvider: AudioProvider, private musicControls: MusicControls, private suratService: AllSuratServiceProvider,private keyboard: Keyboard,private appMinimize: AppMinimize, private localNotifications: LocalNotifications,private dialogprovider :DialogserviceProvider, private backgroundMode: BackgroundMode, private app: App) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.keyboard.onKeyboardShow().subscribe(function(){
        this.reduceScrollAndHideTabs();
      });
      this.keyboard.onKeyboardHide().subscribe(function(){
        this.addScrollAndShowTabs()
      });

      app.viewDidEnter.subscribe(function(callback){
        if(Object.getOwnPropertyNames(callback.data).length===0){
          this.currentView="rootPage"
        }else if(callback.data.title!="Berbagi Aplikasi"){
          this.currentView="childPage"
        }
        if(this.indexToPlay!=-1){
          this.addScrollAndShowTabs();
        }
      });

      this.requestAds(true);

      platform.pause.subscribe(function(){
        this.backgroundMode.enable();
        this.backgroundMode.configure({silent:!0});
        this.requestAds(false);
      });

      platform.resume.subscribe(function(){
        this.requestAds(true);
      });

      this.events.subscribe("murottal:isPlaying",(showNavigation,isDetailSurat,data,typeOfPlay,indexToPlay) => {
        this.showNavigation=showNavigation;
        this.isDetailSurat=isDetailSurat;
        if(isDetailSurat){
          setTimeout(function(){
            this.showProgressInDetail= isDetailSurat;
            this.navigationPlaying="floatingPlayButton";
            this.progressBar="progressBar hideTemporary";
          },300);
        }
        this.allData = data;
        let pathMurottal = window.hasOwnProperty("cordova")?"/android_asset/www/assets/murottal/":"assets/murottal/";
        this.myTracks=[];
        for (let i in this.allData){
          this.myTracks.push({
            src:pathMurottal+this.allData[i].fileName,
            artist:"Syeikh Al Ghomidi",
            title:"Surat "+this.allData[i].suratName,
            indexToPlay:parseInt(i),
            preload:"metadata"
          });
        }

        switch(typeOfPlay){
          case 1:
                this.playRandom();
            break;
          case 2:
                this.previous();
            break;
          case 3:
                this.navigationPlaying="floatingPlayButton height0",
                this.progressBar="progressBar",
                this.indexToPlay=indexToPlay;
                this.audioProvider.stop()
                this.selectedTrack = {},
                this.isRepeat=false,
                this.isRandom=false,
                this.selectedTrack = this.myTracks[this.indexToPlay];
                setTimeout(function(){
                  this.playorpause()
                },100);
            break;
          case 4:
                this.next();
            break;
          case 5:
                this.playRepeat();
            break;
          default:
                this.addScrollAndShowTabs();
                console.log("No typePlaying");
         }
      });

      platform.registerBackButtonAction(function(){
          if(this.currentView  == "rootPage"){
              this.appMinimize.minimize();
          }else{
              app.navPop()
          }
      });

    });
  }

  requestAds(isRequest){
    if(isRequest){
      this.reward = setInterval(()=>{
        this.suratService.showVideoRewardsAd();
      },90000);
      this.interstitial = setInterval(()=>{
        this.suratService.showInterstitialAd();
      },90000);
      this.banner = setInterval(()=>{
        this.suratService.showBannerAd();
      },60000);
    }else{
      clearInterval(this.banner);
      clearInterval(this.interstitial);
      clearInterval(this.reward);
    }
  }

  autoplay(){
    var n=this;
    setTimeout(()=>{
      if(this.audio.isPlaying){
        this.audioProvider.stop();
      }else{
        this.selectedSurat();
        this.iconMediaPlaying="ios-pause";
        this.audioProvider.play(this.audio.id);
        this.createMusicControl();
        this.subscribeMusicControl();
        this.suratService.setRecordPlaying(this.indexToPlay+1);
      }
    },100);
  }
  playRandom(){
    if(this.isRandom){
      this.dialogprovider.toastShow("Putar Surat Acak / Random dimatikan","1000","bottom");
      this.isRandom = false;
    }else{
      this.dialogprovider.toastShow("Putar Surat Acak / Random diaktifkan","1000","bottom");
      this.isRandom = true;
    }
  }
  randomSelected(){
    this.selectedTrack={};
    this.selectedTrack=this.myTracks[this.indexToPlay];
    if(this.tempRandomArray.length<this.myTracks.length){

    }else if(){

    }
  },&&(this.tempRandomArray.push(this.indexToPlay),setTimeout(function(){n.autoplay()},100))},n.prototype.previous=function(){var n=this;this.audioProvider.stop(),this.indexToPlay>0?this.indexToPlay--:this.indexToPlay=this.myTracks.length-1,this.iconMediaPlaying="ios-pause",this.selectedTrack={},this.selectedTrack=this.myTracks[this.indexToPlay],setTimeout(function(){n.autoplay()},100)},n.prototype.playorpause=function(){var n=this;this.selectedSurat(),this.createMusicControl(),this.subscribeMusicControl(),this.audio.isPlaying?setTimeout(function(){n.audioProvider.pause(),n.musicControls.listen(),n.musicControls.updateIsPlaying(!1),n.iconMediaPlaying="ios-play"},10):(setTimeout(function(){n.canPlay=!1,n.audioProvider.play(n.audio.id),n.suratService.setRecordPlaying(n.indexToPlay+1),n.musicControls.listen(),n.musicControls.updateIsPlaying(!0)},10),setTimeout(function(){n.iconMediaPlaying="ios-pause",n.canPlay=!0},50))},n.prototype.next=function(){var n=this;this.audioProvider.stop(),this.indexToPlay<this.myTracks.length-1?this.indexToPlay++:this.indexToPlay=0,this.iconMediaPlaying="ios-pause",this.selectedTrack={},this.selectedTrack=this.myTracks[this.indexToPlay],setTimeout(function(){n.autoplay()},100)},n.prototype.playRepeat=function(){this.isRepeat?(this.dialogprovider.toastShow("Putar Ulang / Repeat dimatikan","1000","bottom"),this.isRepeat=!1):(this.dialogprovider.toastShow("Putar Ulang / Repeat diaktifkan","1000","bottom"),this.isRepeat=!0)},n.prototype.selectedSurat=function(){this.detailSuratContent={suratName:this.allData[this.indexToPlay].suratName,suratMeaning:this.allData[this.indexToPlay].suratMeaning,countAyat:this.allData[this.indexToPlay].countAyat,suratCategory:this.allData[this.indexToPlay].isMakiyah?"Makiyah":"Madaniyah"},this.events.publish("murottal:detailSurat",this.detailSuratContent,!1)},n.prototype.onTrackFinished=function(n){this.canPlay&&(this.isRepeat?(this.audioProvider.play(this.audio.id),this.suratService.setRecordPlaying(this.indexToPlay+1),this.createMusicControl(),this.subscribeMusicControl()):this.isRandom?(this.indexToPlay=Math.floor(Math.random()*this.myTracks.length),-1==this.myTracks.indexOf(this.indexToPlay)?this.randomSelected():(this.indexToPlay=Math.floor(Math.random()*this.myTracks.length),this.randomSelected())):(this.localNotifications.clearAll(),this.localNotifications.schedule({id:this.indexToPlay,title:"Alhamdulillah",text:"Anda telah mendengarkan surat "+this.detailSuratContent.suratName}),this.next()))},n.prototype.closeMurottal=function(){this.suratService.setRecordPlaying(0),this.showNavigation=!1,this.audioProvider.stop(),this.indexToPlay=-1,this.addScrollAndShowTabs()},n.prototype.createMusicControl=function(){this.musicControls.create({track:this.detailSuratContent.suratName,artist:"Syeikh Al Ghomidi",cover:"assets/imgs/icon-alquran-detail.png",isPlaying:!0,dismissable:!0,hasPrev:!0,hasNext:!0,hasClose:!1,ticker:this.detailSuratContent.suratName})},n.prototype.subscribeMusicControl=function(){var n=this;this.musicControls.subscribe().subscribe(function(l){switch(JSON.parse(l).message){case"music-controls-next":n.next();break;case"music-controls-previous":n.previous();break;case"music-controls-pause":case"music-controls-play":n.playorpause();break;case"music-controls-destroy":n.closeMurottal()}})},n.prototype.reduceScrollAndHideTabs=function(){this.navigationPlaying="floatingPlayButton hideTemporary",document.getElementsByClassName("tabbar")[0].classList.add("hideTemporary");for(var n=document.getElementsByClassName("scroll-content"),l=0;l<n.length;l++)document.getElementsByClassName("scroll-content")[l].classList.remove("margin-bottom125"),document.getElementsByClassName("scroll-content")[l].classList.add("margin-bottom0")},n.prototype.addScrollAndShowTabs=function(){document.getElementsByClassName("tabbar")[0].classList.remove("hideTemporary");for(var n=document.getElementsByClassName("scroll-content"),l=0;l<n.length;l++)document.getElementsByClassName("scroll-content")[l].classList.remove("margin-bottom0"),-1==this.indexToPlay?document.getElementsByClassName("scroll-content")[l].classList.remove("margin-bottom125"):(this.navigationPlaying="floatingPlayButton",document.getElementsByClassName("scroll-content")[l].classList.add("margin-bottom125"))},n}()

}
