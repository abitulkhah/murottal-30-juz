import { Keyboard } from '@ionic-native/keyboard';
import { Events } from 'ionic-angular/util/events';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { AudioProvider } from 'ionic-audio';
import { MusicControls } from '@ionic-native/music-controls';
import { AllSuratServiceProvider } from '../providers/all-surat-service/all-surat-service';
import { AppMinimize } from '@ionic-native/app-minimize';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { DialogserviceProvider } from '../providers/dialogservice/dialogservice';
import { BackgroundMode } from '@ionic-native/background-mode';
import { App } from 'ionic-angular/components/app/app';
import { ViewChild } from '@angular/core/src/metadata/di';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  myTracks=[];
  showNavigation = false;
  isDetailSurat = false;
  indexToPlay=-1;
  selectedTrack;
  allData;
  progressBar;
  isRepeat = false;
  isRandom = false;
  iconMediaPlaying = "ios-play";
  tempRandomArray = [];
  navigationPlaying="floatingPlayButton";
  currentView="rootPage";
  @ViewChild("audio") audio;
  banner;
  interstitial;
  reward;
  canPlay = true;
  detailSuratContent;
  showProgressInDetail = false;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private events: Events, private audioProvider: AudioProvider, private musicControls: MusicControls, private suratService: AllSuratServiceProvider,private keyboard: Keyboard,private appMinimize: AppMinimize, private localNotifications: LocalNotifications,private dialogprovider :DialogserviceProvider, private backgroundMode: BackgroundMode, private app: App) {
    platform.ready().then(() => {

      statusBar.styleDefault();
      splashScreen.hide();

      this.keyboard.onKeyboardShow().subscribe(function(){
        this.reduceScrollAndHideTabs();
      });

      this.keyboard.onKeyboardHide().subscribe(function(){
        this.addScrollAndShowTabs()
      });

      app.viewDidEnter.subscribe(function(callback){
        if(Object.getOwnPropertyNames(callback.data).length===0){
          this.currentView="rootPage"
        }else if(callback.data.title!="Berbagi Aplikasi"){
          this.currentView="childPage"
        }
        if(this.indexToPlay!=-1){
          this.addScrollAndShowTabs();
        }
      });

      this.requestAds(true);

      platform.pause.subscribe(function(){
        this.backgroundMode.enable();
        this.backgroundMode.configure({silent:!0});
        this.requestAds(false);
      });

      platform.resume.subscribe(function(){
        this.requestAds(true);
      });

      this.events.subscribe("murottal:isPlaying",(showNavigation,isDetailSurat,data,typeOfPlay,indexToPlay) => {
        this.showNavigation=showNavigation;
        this.isDetailSurat=isDetailSurat;
        if(isDetailSurat){
          setTimeout(()=>{
            this.showProgressInDetail= isDetailSurat;
          },300);
        }else{
          this.navigationPlaying="floatingPlayButton";
          this.progressBar="progressBar hideTemporary";
          this.showProgressInDetail= isDetailSurat;
        }

        this.allData = data;
        let murottalPath = window.hasOwnProperty("cordova")?"/android_asset/www/assets/murottal/":"assets/murottal/";
        this.myTracks=[];

        for (let i in this.allData){
          this.myTracks.push({
            src:murottalPath+this.allData[i].fileName,
            artist:"Syeikh Al Ghomidi",
            title:"Surat "+this.allData[i].suratName,
            indexToPlay:parseInt(i),
            preload:"metadata"
          });
        }

        switch(typeOfPlay){
          case 1:
                this.playRandom();
            break;
          case 2:
                this.previous();
            break;
          case 3:
                this.navigationPlaying="floatingPlayButton height0",
                this.progressBar="progressBar",
                this.indexToPlay=indexToPlay;
                this.audioProvider.stop()
                this.selectedTrack = {},
                this.isRepeat=false,
                this.isRandom=false,
                this.selectedTrack = this.myTracks[this.indexToPlay];
                setTimeout(function(){
                  this.playorpause()
                },100);
            break;
          case 4:
                this.next();
            break;
          case 5:
                this.playRepeat();
            break;
          default:
                this.addScrollAndShowTabs();
                console.log("No typePlaying");
            break;
         }
      });

      platform.registerBackButtonAction(function(){
          if(this.currentView == "rootPage"){
              this.appMinimize.minimize();
          }else{
              app.navPop();
          }
      });

    });
  }

  requestAds(isRequest){
    if(isRequest){
      this.reward = setInterval(()=>{
        this.suratService.showVideoRewardsAd();
      },90000);
      this.interstitial = setInterval(()=>{
        this.suratService.showInterstitialAd();
      },90000);
      this.banner = setInterval(()=>{
        this.suratService.showBannerAd();
      },60000);
    }else{
      clearInterval(this.banner);
      clearInterval(this.interstitial);
      clearInterval(this.reward);
    }
  }

  autoplay(){
    setTimeout(()=>{
      if(this.audio.isPlaying){
        this.audioProvider.stop();
      }else{
        this.selectedSurat();
        this.iconMediaPlaying="ios-pause";
        this.audioProvider.play(this.audio.id);
        this.createMusicControl();
        this.subscribeMusicControl();
        this.suratService.setRecordPlaying(this.indexToPlay+1);
      }
    },100);
  }

  playRandom(){
    if(this.isRandom){
      this.dialogprovider.toastShow("Putar Surat Acak / Random dimatikan","1000","bottom");
      this.isRandom = false;
    }else{
      this.dialogprovider.toastShow("Putar Surat Acak / Random diaktifkan","1000","bottom");
      this.isRandom = true;
    }
  }

  randomSelected(){
    this.selectedTrack={};
    this.selectedTrack=this.myTracks[this.indexToPlay];
    if(this.tempRandomArray.length<this.myTracks.length){
      this.tempRandomArray.push(this.indexToPlay);
      setTimeout(()=>{
        this.autoplay();
      },100)
    }
  }

  previous(){
    this.audioProvider.stop();
    this.iconMediaPlaying="ios-pause";
    this.selectedTrack = {};

    if(this.indexToPlay>0){
      this.indexToPlay--;
    } else {
      this.indexToPlay = this.myTracks.length-1;
    }

    this.selectedTrack = this.myTracks[this.indexToPlay];
    setTimeout(()=>{
      this.autoplay();
    },100);
  }

  playorpause (){
    this.selectedSurat();
    this.createMusicControl();
    this.subscribeMusicControl();
    if(this.audio.isPlaying){
      setTimeout(()=>{
        this.audioProvider.pause();this.musicControls.listen();
        this.musicControls.updateIsPlaying(false);
        this.iconMediaPlaying="ios-play";
      },10);
    }else{
      setTimeout(()=>{
        this.canPlay = false;
        this.audioProvider.play(this.audio.id);
        this.suratService.setRecordPlaying(this.indexToPlay+1);
        this.musicControls.listen();
        this.musicControls.updateIsPlaying(true);
      },10);
      setTimeout(()=>{
        this.iconMediaPlaying = "ios-pause";
        this.canPlay = true;
      },50);
    }
  }

  next(){
    this.audioProvider.stop();
    if(this.indexToPlay<this.myTracks.length-1){
      this.indexToPlay++;
    }else{
      this.indexToPlay=0;
    }
    this.iconMediaPlaying="ios-pause";
    this.selectedTrack={};
    this.selectedTrack=this.myTracks[this.indexToPlay];
    setTimeout(()=>{
      this.autoplay();
    },100)
  }

  playRepeat(){
    if(this.isRepeat){
      this.dialogprovider.toastShow("Putar Ulang / Repeat dimatikan","1000","bottom");
      this.isRepeat=false;
    }else{
      this.dialogprovider.toastShow("Putar Ulang / Repeat diaktifkan","1000","bottom");
      this.isRepeat=true;
    }
  }

  selectedSurat(){
    this.detailSuratContent = {
      suratName:this.allData[this.indexToPlay].suratName,
      suratMeaning:this.allData[this.indexToPlay].suratMeaning,
      countAyat:this.allData[this.indexToPlay].countAyat,
      suratCategory:this.allData[this.indexToPlay].isMakiyah?"Makiyah":"Madaniyah"
    };
    this.events.publish("murottal:detailSurat",this.detailSuratContent,false);
  }

  onTrackFinished(tracks){
    if(this.canPlay){
      if(this.isRepeat){
        this.audioProvider.play(this.audio.id);
        this.suratService.setRecordPlaying(this.indexToPlay+1);
        this.createMusicControl();
        this.subscribeMusicControl();
      }else if(this.isRandom){
        this.indexToPlay=Math.floor(Math.random()*this.myTracks.length);
        if(this.myTracks.indexOf(this.indexToPlay)==-1){
          this.randomSelected();
        }else{
          this.indexToPlay = Math.floor(Math.random()*this.myTracks.length);
          this.randomSelected();
        }
      }else{
        this.localNotifications.clearAll();
        this.localNotifications.schedule({
          id:this.indexToPlay,
          title:"Alhamdulillah",
          text:"Anda telah mendengarkan surat "+this.detailSuratContent.suratName
        });
        this.next();
      }
    }
  }

  closeMurottal(){
    this.suratService.setRecordPlaying(0);
    this.showNavigation=false;
    this.audioProvider.stop();
    this.indexToPlay=-1;
    this.addScrollAndShowTabs();
  }

  createMusicControl(){
    this.musicControls.create({
      track : this.detailSuratContent.suratName,
      artist : "Syeikh Al Ghomidi",
      cover : "assets/imgs/icon-alquran-detail.png",
      isPlaying : true,
      dismissable : true,
      hasPrev : true,
      hasNext : true,
      hasClose : false,
      ticker : this.detailSuratContent.suratName
    })
  }

  subscribeMusicControl(){
    this.musicControls.subscribe().subscribe((data)=>{
      switch(JSON.parse(data).message){
        case "music-controls-next":
              this.next();
            break;
        case "music-controls-previous":
              this.previous();
            break;
        case "music-controls-pause":
        case "music-controls-play":
              this.playorpause();
            break;
        case "music-controls-destroy":
              this.closeMurottal()
            break;
      }
    });
  }

  reduceScrollAndHideTabs(){
    this.navigationPlaying="floatingPlayButton hideTemporary";
    document.getElementsByClassName("tabbar")[0].classList.add("hideTemporary");
    let scrollContent = document.getElementsByClassName("scroll-content");
    for(var i=0; i<scrollContent.length; i++){
      document.getElementsByClassName("scroll-content")[i].classList.remove("margin-bottom125");
      document.getElementsByClassName("scroll-content")[i].classList.add("margin-bottom0")
    }
  }

  addScrollAndShowTabs(){
    document.getElementsByClassName("tabbar")[0].classList.remove("hideTemporary");
    let scrollContent = document.getElementsByClassName("scroll-content");
    for(var i=0; i<scrollContent.length; i++){
      document.getElementsByClassName("scroll-content")[i].classList.remove("margin-bottom0");
      if(this.indexToPlay==-1){
        document.getElementsByClassName("scroll-content")[i].classList.remove("margin-bottom125");
      }else{
        this.navigationPlaying="floatingPlayButton",document.getElementsByClassName("scroll-content")[i].classList.add("margin-bottom125");
      }
    }
  }

}
